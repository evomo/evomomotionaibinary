//
//  EVOClassification.h
//  EVOClassification
//
//  Created by Jakob Wowy on 25.01.20.
//  Copyright © 2020 Jakob Wowy. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for EVOClassification.
FOUNDATION_EXPORT double EVOClassificationVersionNumber;

//! Project version string for EVOClassification.
FOUNDATION_EXPORT const unsigned char EVOClassificationVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <EVOClassification/PublicHeader.h>


